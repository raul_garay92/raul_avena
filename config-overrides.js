/*
 * @Author: Alejandro Leonor
 * @Date: 2018-03-16 11:51:29
 * @Last Modified by: Alejandro Leonor
 * @Last Modified time: 2018-03-25 03:07:20
 */

const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

// Custom configuration for webpack
module.exports = function override(config, env) {
  config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config);
  config = rewireLess.withLoaderOptions({
    modifyVars: {
      '@primary-color': '#263e5c',
      '@body-backgroud': '#fff',
      '@component-background': '#f6f6f6',
      '@input-bg': '#f6f6f6',
      '@border-color-base': '#f6f6f6',
      '@menu-collapsed-width': '64px',
    },
  })(config, env);
  return config;
};

