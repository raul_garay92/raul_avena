/*
 * @Author: Jorge García
 * @Date: 2018-05-22
 * @Last Modified by: Brandon Leon
 * @Last Modified date: 2018-05-29
 */

import { isValidRfc } from '../src/routes/register/helpers/utils';

test('BLABLABLA is NOT a valid RFC', () => {
  expect(isValidRfc('BLABLABLA')).toBe(false);
});

test('GADJ890508JHA is a valid RFC', () => {
  expect(isValidRfc('GADJ890508JHA')).toBe(true);
})

