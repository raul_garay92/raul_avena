/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */

import { notification } from 'antd';
import { parseApiError } from '../error';

export const notificationError = (error) => {
  notification.destroy();
  return (
    notification.error({
      message: `Error ${parseApiError(error).status}`,
      description: parseApiError(error).message,
      duration: 0,
    })
  );
};
