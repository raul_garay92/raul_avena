/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */
export const parseApiError = (error) => {
  try {
    const parsedError = JSON.parse(JSON.stringify(error));
    return {
      status: parsedError.response.status,
      message: parsedError.response.data.message || parsedError.response.data.error_description,
    };
  } catch (ex) {
    return {
      status: 500,
      message: 'Something went wrong!',
    };
  }
};
