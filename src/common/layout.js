/*
 * @Author by: Raul Garay
 * @Date: 2018-12-20
 */

import React, { Component } from 'react';
import { Layout, Row, Col } from 'antd';
import './styles.css';
import logo from '../assets/footer-logo.png';

const { Header, Content, Footer } = Layout;
class MainLayout extends Component {
  state = {}
  render() {
    return (
      <Layout>
        <Header
          className="grad"
        >
          <Col span={4} >
            <a href="/">
              <img src={logo} alt="logo" style={{ height: 31 }} />
            </a>
          </Col>

        </Header>
        <Content style={{ padding: '0 50px', marginTop: 64 }}>
          <Row type="flex" justify="center">
            <Col className="shadow" span={22} >
              {this.props.children}
            </Col>
          </Row>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Avena.io challenge ©2018 Created by Raul Garay
        </Footer>
      </Layout>
    );
  }
}
export default MainLayout;
