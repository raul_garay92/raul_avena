import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configure';

import Routes from './routes';
import registerServiceWorker from './registerServiceWorker';

const App = () => (
  <Provider store={configureStore()}>
    <Routes />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
