/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */
import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { rootEpic, rootReducer } from './modules';

const epicMiddleware = createEpicMiddleware(rootEpic);
const composeEnhacers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line

const configureStore = () => {
  const store = createStore(
    rootReducer,
    composeEnhacers(applyMiddleware(epicMiddleware)),
  );

  return store;
};

export default configureStore;
