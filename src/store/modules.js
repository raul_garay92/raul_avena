/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */
import { combineEpics } from 'redux-observable';
import { combineReducers } from 'redux';
import * as Ingredients from '../routes/ingredients/redux';

export const rootEpic =
  combineEpics(Ingredients.epics);

export const rootReducer = combineReducers({
  ingredients: Ingredients.reducer,
});
