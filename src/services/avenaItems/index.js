/*
 * @Author: Raul Garay
 * @Date: 2018-09-19
 */

import axios from 'axios';

const instance = axios.create();

export const getItems = ({ limit, search, nextPageToken }) => instance.get(encodeURI(`https://avena-dev.appspot.com/_ah/api/food/v1/food/smae?limit=${limit}&search=${search}&nextPageToken=${nextPageToken}`)).then(data => data.data);

instance.interceptors.request.use((conf) => {
  const configuration = conf;
  configuration.auth = {
    username: 'test@test.test',
    password: 'test',
  };
  return configuration;
}, (error) => { Promise.reject(error); });
