import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Input, Form, Row, Icon } from 'antd';
import Card from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import history from '../../common/routerHistory';

import {
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
  formatFormData,
} from './utils';
import './styles.css';

class Checkout extends Component {
  state = {
    number: '',
    name: '',
    expiry: '',
    cvc: '',
    focused: '',
    formData: null,
  };

  handleInputFocus = ({ target }) => {
    this.setState({
      focused: target.id,
    });
  };

  handleInputChange = ({ target }) => {
    if (target.id === 'number') {
      target.value = formatCreditCardNumber(target.value);
    } else if (target.id === 'expiry') {
      target.value = formatExpirationDate(target.value);
    } else if (target.id === 'cvc') {
      target.value = formatCVC(target.value);
    }

    this.setState({ [target.id]: target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ formData: values });
      }
    });
    this.props.form.resetFields();
  };

  renderCheckoutItem = () => {
    const { checkoutItem: { name, type } } = this.props;
    return (
      <Row style={{ marginBottom: 40 }}>
        <Row>
          <h2><b>Nombre: </b>{name}</h2>
        </Row>
        <Row>
          <h2><b>Tipo: </b>{type}</h2>
        </Row>

      </Row>
    );
  }

  render() {
    const {
      name, number, expiry, cvc, focused, formData,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <Row key="Payment">
        <Row>
          <Button
            type="primary"
            onClick={() => {
              history.goBack();
            }}
          >
            <Icon type="left" />
            Regresar
          </Button>
        </Row>
        <Row className="App-payment">
          <h1>Artículo a pagar</h1>
          {this.renderCheckoutItem()}
          <Card
            number={number}
            name={name}
            expiry={expiry}
            cvc={cvc}
            focused={focused}
          />
          <Row style={{ marginTop: 30 }}>
            <Form onSubmit={this.handleSubmit} style={{ margin: 'auto' }}>
              <Form.Item
                style={{ alignSelf: 'center' }}
                label="Numero de tarjeta"
              >
                {getFieldDecorator('number', {
                  rules: [{
                    required: true, message: 'Ingresa tu número de tarjeta!',
                  },
                  ],
                })(<Input
                  onFocus={this.handleInputFocus}
                  onChange={this.handleInputChange}
                />)}
              </Form.Item>

              <Form.Item
                label="Nombre"
              >
                {getFieldDecorator('name', {
                  rules: [{
                    required: true, message: 'Ingresa tu nombre tal y como aparece en la tarjeta!',
                  }],
                })(<Input
                  onFocus={this.handleInputFocus}
                  onChange={this.handleInputChange}
                />)}
              </Form.Item>

              <Form.Item
                label="Fecha de expiración"
              >
                {getFieldDecorator('expiry', {
                  rules: [{
                    required: true, message: 'Ingresa la fecha de expiracion!',
                  },
                  { pattern: '^([0-9]{2}/[0-9]{2})', message: 'Ingresa un cvc valido' }],
                })(<Input
                  onFocus={this.handleInputFocus}
                  onChange={this.handleInputChange}
                />)}
              </Form.Item>

              <Form.Item
                label="CVC"
              >
                {getFieldDecorator('cvc', {
                  rules: [{
                    required: true, message: 'Ingresa tu numero de tarjeta!',
                  },
                  { pattern: '^[0-9]{3,4}', message: 'Ingresa un cvc valido' }],
                })(<Input
                  onFocus={this.handleInputFocus}
                  onChange={this.handleInputChange}
                />)}
              </Form.Item>
              <Button type="primary" htmlType="submit">Pagar</Button>
            </Form>
          </Row>
          <Row>
            {formData && (
              <Row className="App-highlight">
                {formatFormData(formData).map((d, i) => <Row key={i}>{d}</Row>)}
              </Row>
            )}
          </Row>

        </Row>
      </Row>
    );
  }
}

const mapStateToProps = ({ ingredients }) => ingredients;

export default connect(mapStateToProps, {})(Form.create()(Checkout));

