/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */

import React from 'react';
import { Router, Route } from 'react-router-dom';
import MainLayout from '../common/layout';
import history from '../common/routerHistory';

import Ingredients from './ingredients';
import Checkout from './checkout';

const routes = [
  {
    key: 'ingredients',
    path: '/',
    component: Ingredients,
  },
  {
    key: 'checkout',
    path: '/checkout',
    component: Checkout,
  },
];

function renderRoutes(routesToMap) {
  return routesToMap.map(route =>
    (<Route exact key={route.key} path={route.path} component={route.component} />));
}

export default () => (
  <Router history={history}>
    <MainLayout>
      {renderRoutes(routes)}
    </MainLayout>
  </Router>
);
