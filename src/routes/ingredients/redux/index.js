/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */

import { createReducer } from 'aleonor-redux-helpers';
import { isEmpty } from 'lodash';
import epics from './epics';
import {
  FETCH_FILTERED_PRODUCTS, FETCH_DATA_FULLFILLED, SET_SEARCH_TEXT,
  SET_CHECKOUT_ITEM, CLEAN_ITEMS,
} from './types';

const defaultState = {
  reducerKey: 'ingredients',
  itemList: [],
  nextPageToken: '',
  limit: 10,
  search: '',
  checkoutItem: {},
};


// Actions
const fetchItems = filter => ({ type: FETCH_FILTERED_PRODUCTS, filter });

const cleanItems = () => ({ type: CLEAN_ITEMS });

const setSearchText = searchText => ({ type: SET_SEARCH_TEXT, payload: searchText });

const setCheckoutItem = item => ({ type: SET_CHECKOUT_ITEM, payload: item });

const fetchDataFullfilled = items => ({ type: FETCH_DATA_FULLFILLED, payload: items });

// Reducer handling action types
const { reducer, defaultActions } = createReducer(defaultState, {
  [FETCH_DATA_FULLFILLED]: (state, { items = [], nextPageToken }) => {
    let newToken = nextPageToken;
    if (isEmpty(items)) {
      newToken = '';
    }
    return ({ ...state, itemList: [...state.itemList, ...items], nextPageToken: newToken });
  },
  [SET_SEARCH_TEXT]: (state, payload) =>
    ({ ...state, search: payload }),
  [SET_CHECKOUT_ITEM]: (state, payload) =>
    ({ ...state, checkoutItem: payload }),
  [CLEAN_ITEMS]: state =>
    ({ ...state, itemList: [] }),
});

const actions = {
  ...defaultActions,
  fetchDataFullfilled,
  fetchItems,
  setSearchText,
  setCheckoutItem,
  cleanItems,
};

export {
  epics,
  reducer,
  actions,
};
