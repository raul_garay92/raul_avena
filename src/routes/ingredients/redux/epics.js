/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */
import { combineEpics } from 'redux-observable';
import { Observable } from 'rxjs';
import { actions } from './index';
import { FETCH_FILTERED_PRODUCTS } from './types';
import { notificationError } from '../../../common/helpers/utils';
import * as avenaItemsService from '../../../services/avenaItems';

const getItems = action$ =>
  action$.ofType(FETCH_FILTERED_PRODUCTS)
    .mergeMap(action =>
      Observable.concat(
        Observable.of(actions.loading(true)),
        Observable.fromPromise(avenaItemsService.getItems(action.filter))
          .flatMap(response =>
            Observable.concat(
              Observable.of(actions.loading(false)),
              Observable.of(actions.fetchDataFullfilled(response)),
            ))
          .catch(error =>
            Observable.concat(
              Observable.of(actions.loading(false)),
              Observable.of(actions.showMessage(() => {
                notificationError(error);
              })),
            )),
      ));

export default combineEpics(getItems);
