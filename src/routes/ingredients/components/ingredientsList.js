
/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */

import React, { Component } from 'react';
import { List, Avatar, Button, Input, Col, Row } from 'antd';

const { Search } = Input;

const Macronutrient = ({ type, grams = '0.0' }) => (
  <span>{`${type} ${grams}g`}</span>
);

class IngredientsList extends Component {
  renderDescription = (item) => {
    const { type, unity, suggestion } = item;
    return (
      <Row>
        <Row>
          {type}
        </Row>
        <Row>Porción: {suggestion} {unity}</Row>
      </Row>
    );
  }

  renderExtraInfo = (item) => {
    const { energyCal = 0 } = item;
    return (
      <Row>
        <Row>
          <Row>
            Calorias por porción
          </Row>
          <Row>{energyCal}</Row>
        </Row>
        <Row style={{ marginTop: 30 }}>
          <h5>Comprar</h5>
          <Button
            type="primary"
            icon="shopping-cart"
            onClick={() => {
              this.props.setCheckoutItem(item);
            }}
          />
        </Row>
      </Row>
    );
  }

  render() {
    const {
      itemList, fetchIngredients, handleSearch, showLoadMore,
      filterTag,
    } = this.props;
    return (
      <Row>
        <h1>Lista de ingredientes</h1>
        <Row type="flex" justify="center">
          <Col span={15}>
            <Search
              placeholder="Buscar ingredientes"
              id="itemSearchInput"
              onSearch={handleSearch}
              enterButton
            />
          </Col>
          <Col>
            {filterTag()}
          </Col>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <List
            itemLayout="vertical"
            size="large"
            dataSource={itemList}
            renderItem={item => (
              <List.Item
                key={item.name}
                actions={[<Macronutrient type="Carbohidratos" grams={item.carbs} />, <Macronutrient type="Proteina" grams={item.protein} />, <Macronutrient type="Grasas" grams={item.lipids} />]}
                extra={this.renderExtraInfo(item)}
              >
                <List.Item.Meta
                  avatar={<Avatar src={item.avatar} />}
                  title={<a href={item.href}>{item.name}</a>}
                  description={this.renderDescription(item)}
                />
                {item.content}
              </List.Item>
            )}
          />
        </Row>
        <Row>
          {showLoadMore ?
            (
              <Button
                type="primary"
                onClick={fetchIngredients}
              >
                Cargar más
              </Button>
            )
            : null}

        </Row>
      </Row>
    );
  }
}


export default IngredientsList;
