
/*
 * @Author: Raul Garay
 * @Date: 2018-12-20
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { Row, Tag } from 'antd';
import { actions } from './redux';
import history from '../../common/routerHistory';
import IngredientsList from './components/ingredientsList';

class Ingredients extends Component {
  componentDidMount() {
    this.fetchIngredients();
  }

  setCheckOutItem = (item) => {
    const {
      setCheckoutItem,
    } = this.props;
    setCheckoutItem(item);
    history.push('/checkout');
  }

  fetchIngredients = () => {
    const {
      fetchItems, nextPageToken, limit, search,
    } = this.props;
    fetchItems({ nextPageToken, limit, search });
  }

  handleSearch = (value) => {
    const {
      setSearchText, fetchItems, limit, cleanItems,
    } = this.props;
    const searchText = value.trim();
    setSearchText(searchText);
    cleanItems();
    fetchItems({ nextPageToken: '', limit, search: searchText });
  }

  resetStore = () => {
    const {
      setSearchText, fetchItems, limit, cleanItems,
    } = this.props;
    setSearchText('');
    cleanItems();
    fetchItems({ nextPageToken: '', limit, search: '' });
    document.getElementById('itemSearchInput').value = '';
  }

  renderFilter = () => {
    const { search } = this.props;
    if (!isEmpty(search)) {
      return (
        <Tag closable style={{ marginLeft: 10 }} onClose={this.resetStore}>{search}</Tag>
      );
    }
    return null;
  }

  render() {
    const { itemList, nextPageToken } = this.props;
    const showLoadMore = !isEmpty(nextPageToken);
    return (
      <Row>
        <IngredientsList
          itemList={itemList}
          fetchIngredients={this.fetchIngredients}
          handleSearch={this.handleSearch}
          setCheckoutItem={this.setCheckOutItem}
          showLoadMore={showLoadMore}
          filterTag={this.renderFilter}
        />
      </Row>
    );
  }
}

const mapStateToProps = ({ ingredients }) => ingredients;

export default connect(mapStateToProps, actions)(Ingredients);
